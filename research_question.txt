Group Name : 68
Question 
========
 
RQ : Is there a difference in proportions of Covid-19 infection between men and women in South Korea?

Null Hypothesis : There is no difference in proportions of Covid-19 infection between men and women in South Korea.

Alternative Hypothesis : There is a difference in proportions of Covid-19 infection between men and women in South Korea.


Dataset
=======
URL : https://www.kaggle.com/kimjihoo/coronavirusdataset?select=PatientInfo.csv


Column Headings : 

.....

Colnames(PatientInfo)

 [1] "patient_id"         "sex"                "age"              
 [4] "country"            "province"           "city"              
 [7] "infection_case"     "infected_by"        "contact_number"    
[10] "symptom_onset_date" "confirmed_date"     "released_date"    
[13] "deceased_date"      "state"  
 
.......

 